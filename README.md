CARL
====

CARL (Command-line Argument Reasonable Library) is an argument parsing and
subcommand library. It is built on top of `argparse` for instant awesomeness,
but brings an event nicer interface to it.


Why on earth "CARL"?
====================

So you have noticed the acronym is a backronym. Ok. Well, there are a few
nice Carls throughout history, from astronomers to murderous llamas. Pick
your favorite one and pretend it's because of him.
