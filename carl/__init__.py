'''Carl is a module for creating entry-point functions that can parse arguments
from the command line.
'''

from .carl import command, Arg, NotArg, REQUIRED, STOP
